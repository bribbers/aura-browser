[Desktop Entry]
Name=AuraBrowser
Name[en_GB]=AuraBrowser
MimeType=@DesktopMimeType@
Exec=@DesktopExec@
Icon=aura-browser
Type=Application
NoDisplay=@DesktopNoDisplay@
GenericName=Aura Browser
Categories=Qt;KDE;Browser;Internet;
Keywords=program;software;Internet;
